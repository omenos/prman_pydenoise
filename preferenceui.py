#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import platform

from PyQt5.QtCore import Qt, QSettings
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import (QWidget, QVBoxLayout, QHBoxLayout, QLabel,
                             QLineEdit, QPushButton, QFileDialog)


class PreferenceApp(QWidget):
    def __init__(self):
        super().__init__()
        self.OS = platform.system()
        self.pref_init_ui()
        self.set_preferences()
        self.setWindowModality(Qt.ApplicationModal)

    def pref_init_ui(self):
        self.pref_layout = QVBoxLayout()
        self.setLayout(self.pref_layout)

        # Basic Options
        self.rman_label = QLabel('RMANTREE')
        self.rman_label.setToolTip('If the RMANTREE environment variable \n'
                                   'is not set then put the path to the \n'
                                   'RenderMan Pro Server directory here')
        self.rman_label.setToolTipDuration(8000)
        self.rman_line = QLineEdit()
        self.rman_line.setMaximumWidth(250)
        self.rman_line.setReadOnly(1)
        self.rman_line.setContextMenuPolicy(Qt.PreventContextMenu)
        self.rman_browse = QPushButton('...')
        self.rman_browse.setFixedWidth(35)

        self.rman_layout = QHBoxLayout()
        self.rman_layout.addWidget(self.rman_label)
        self.rman_layout.addWidget(self.rman_line)
        self.rman_layout.addWidget(self.rman_browse)

        self.filter_label = QLabel('Denoise Filters')
        self.filter_label.setToolTip('If denoise configs are stored in \n'
                                     'the non-default location, set that \n'
                                     'path here')
        self.filter_label.setToolTipDuration(8000)
        self.filter_line = QLineEdit()
        self.filter_line.setMaximumWidth(250)
        self.filter_line.setReadOnly(1)
        self.filter_line.setContextMenuPolicy(Qt.PreventContextMenu)
        self.filter_browse = QPushButton('...')
        self.filter_browse.setFixedWidth(35)

        self.filter_layout = QHBoxLayout()
        self.filter_layout.addWidget(self.filter_label)
        self.filter_layout.addWidget(self.filter_line)
        self.filter_layout.addWidget(self.filter_browse)

        # Directory Selection:
        self.rman_browse.clicked.connect(lambda: self.environment_browse(self.rman_line))
        self.filter_browse.clicked.connect(lambda: self.environment_browse(self.filter_line))

        self.pref_layout.addLayout(self.rman_layout)
        self.pref_layout.addLayout(self.filter_layout)

        self.save_btn = QPushButton('Save Settings')
        self.clear_btn = QPushButton('Clear')

        self.actions_layout = QHBoxLayout()
        self.actions_layout.addWidget(self.save_btn)
        self.actions_layout.addWidget(self.clear_btn)
        self.pref_layout.addLayout(self.actions_layout)

        # Window Creation
        self.setFixedSize(400, 125)
        self.setWindowTitle('PyDenoise Preferences')
        self.setWindowIcon(QIcon('renderman.ico'))

    def set_preferences(self):
        self.SETTINGS = QSettings('Michael Rochefort', 'RenderMan PyDenoise')

    def environment_browse(self, pref_value):
        if len(pref_value.text()) > 0:
            tempdir = pref_value.text()
        elif self.OS == 'Windows':
            tempdir = 'C:\\Program Files\\Pixar'
        elif self.OS == 'Linux':
            tempdir = '/opt/pixar'
        elif self.OS == 'Darwin':
            tempdir = '/Applications/Pixar'

        directory =\
            QFileDialog.getExistingDirectory(caption='PyDenoise Browser',
                                                     directory=tempdir)

        if directory == '':
            pass
        else:
            pref_value.setText(directory)
